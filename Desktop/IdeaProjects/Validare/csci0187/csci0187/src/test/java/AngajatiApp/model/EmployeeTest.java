package AngajatiApp.model;

import org.junit.jupiter.api.*;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

class EmployeeTest {
    Employee e1, e2, e3;

    @BeforeEach
    public void Setup(){
        e1 = new Employee();
        e2 = null;
        e3 = new Employee();
        e3.setFirstName("Ana");
        System.out.println("Before Test");
    }

    @Test
    void getFirstName_simple1() {
        assertEquals("Ana", e3.getFirstName());
        System.out.println("e3 is Ana");
    }

    @Test

    void getFirstName_simple2() {
        try {
            e2.getFirstName();
            assert(false); }
        catch(Exception e)
        {
            System.out.println(e.toString());
            assert(true);
        }
        System.out.println("e2 doesn't have a name");

    }

    @Test
    void getFirstName_simple3() {
        assertEquals("",e1.getFirstName());
        System.out.println("e1 has emtpy FirstName");

    }

    @Test
    void setFirstName() {
        assertEquals("", e1.getFirstName());
        e1.setFirstName("Diana");
        assertNotEquals("",e1.getFirstName());
        assertEquals("Diana", e1.getFirstName());
        System.out.println("set FirstName ok");
    }

    @Test
    void constrEmployee() {
        Employee aux = new Employee();
        assertEquals(e1.getFirstName(), aux.getFirstName());
        assertNotEquals(null, aux);
        System.out.println("constr test ok");
    }

    @AfterEach
    public void TearDown(){
        e1 = null;
        e2 = null;
        e3 = null;
        System.out.println("After Test");
    }

    @Test
    @Timeout(value = 2, unit = TimeUnit.SECONDS)
    public void hasSameFirstName() {
        try {
            //Thread.sleep(100);
            TimeUnit.SECONDS.sleep(1);  //il pun fortat sa doarma 1 sec
        } catch (InterruptedException e ) {
            e.printStackTrace();

        }
        assertEquals( e3.getFirstName(), "Ana");
    }

    //timeout: varianta cu metoda assertTimeout
    @Test
    public void cautaDupaNume() {
        Assertions.assertTimeout(Duration.ofSeconds(10), ()->{delay_and_call();}, String.valueOf(true));
    }

    private void delay_and_call() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace(); }
        e3.getFirstName();
    }

    //aruncarea unei exceptii - metoda assertThrows
    @Order(1)
    @Test
    public void testConstructor2() {
        assertThrows(NullPointerException.class, ()-> {e2.getFirstName();});
        System.out.println("Aruncare exceptii ok");
    }
    }
