package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static AngajatiApp.controller.DidacticFunction.ASISTENT;
import static AngajatiApp.controller.DidacticFunction.TEACHER;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest2 {


    @Test
    void modifyEmployeeFunction_TC01() throws EmployeeException {
        EmployeeMock employeeMock = new EmployeeMock(0);
        List<Employee> employeeList = employeeMock.getEmployeeList();

        assertEquals(0, employeeList.size());
    }

    @Test
    void modifyEmployeeFunction_TC02(){
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee employee = employeeList.get(0);
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());
        employeeMock.modifyEmployeeFunction(employee, ASISTENT);
        assertEquals(employeeList,employeeMock.getEmployeeList());
        System.out.println("functie neschimbata");
    }

    @Test
    void modifyEmployeeFunction_TC03() {
        EmployeeMock employeeMock = new EmployeeMock();
        List<Employee> employeeList = employeeMock.getEmployeeList();

        Employee employee = employeeList.get(0);

        System.out.println("initial function was ASISTENT");
        assertEquals(DidacticFunction.ASISTENT, employee.getFunction());

        employeeMock.modifyEmployeeFunction(employee, TEACHER);

        System.out.println("modified function is TEACHER");
        assertEquals(DidacticFunction.TEACHER, employeeMock.getEmployeeList().get(0).getFunction());

    }


}
