package ro.ubb.c2.reflection;

public class Student {
    private String name;
    private int groupNumber;

    public Student() {}

    public Student(String name, int groupNumber) {
        this.name = name;
        this.groupNumber = groupNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", groupNumber=" + groupNumber +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }
}
