package ro.ubb.c2;

import ro.ubb.c2.reflection.Student;

import java.lang.reflect.Field;

/*
 * 1.
 * - create a new "ro.ubb.reflection.Student" instance (reflection).
 * - initialize the student's private attributes ("name", "groupNumber") with the values ("john", 123).
 * - print the student instance.
 * <p>
 * 2.
 * - create a new "ro.ubb.reflection.Student" instance (reflection)
 * - invoke setName("john") and setGroupNumber(123)
 * - print the student instance.
 * <p>
 * 3.
 * - create a new "ro.ubb.reflection.Student" instance ("john",123) by invoking the constructor * (reflection)
 * - print the student instance.
 * <p>
 * 4.
 * - create a new "ro.ubb.reflection.Employee". ("ro.ubb.reflection.Employee" extends "ro.ubb.reflection.Person", Person has a
 * name, Employee has a salary) (reflection)
 * - set the "name" to "Mary" and the "salary" to 1000;
 * - print the employee
 * <p>
 * 5.
 * - given a Student instance ("John",123), print all attribute names, types, and values.
 */
public class Main {
    public static void main(String[] args) {
        //System.out.println("hello");
        problem1();
    }

    private static void problem1() {
        try {
            Class<?> studentClass = Class.forName("ro.ubb.c2.reflection.Student");
            Object student = studentClass.newInstance();

            Field name = studentClass.getDeclaredField("name");
            name.setAccessible(true);
            name.set(student, "john");

            Field groupNumber = studentClass.getDeclaredField("groupNumber");
            groupNumber.setAccessible(true);
            groupNumber.setInt(student, 123);

            System.out.println(student);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
